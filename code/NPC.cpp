#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */

const float NPC::NPC_COOLDOWN_MAX = 0.5f;

NPC::NPC() : Sprite()
{
    state = IDLE;
    speed = 50.0f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Could pass these in to change the difficulty
    // of a NPC for each level etc. 
    maxRange = 70.0f;
    timeToTarget = 0.25f;
    health = 100;
    points = 10;
    NPCcooldownTimer = 0;
}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/enemyWdeath.png");

    //postion
    Vector2f Position;

    int doubleWidth = 800 * 1.2f;
    int doubleHeight = 800 * 1.2f;

    // get a random number between 0 and
    // 2 x screen size.
    int xCoord = rand() % doubleWidth;
    int yCoord = rand() % doubleHeight;

    // if its on screen move it off.
    if (xCoord < 800)
        xCoord *= -1;

    if (yCoord < 800)
        yCoord *= -1;

    Position.setX(xCoord);
    Position.setY(yCoord);

    // Call sprite constructor
    Sprite::init(renderer, path, 6, &Position);

    // Setup the animation structure
    animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DEAD]->init(10, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 4);

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

/**
 * ~NPC
 * 
 * Destroys the NPC and any associated 
 * objects 
 * 
 */
NPC::~NPC()
{

}

void NPC::update(float dt)
{
    if (state != DEAD) //don't move dead sprite
        ai();
    else
    {
        velocity->setX(0.0f);
        velocity->setY(0.0f);
    }
    NPCcooldownTimer += dt;

    Sprite::update(dt);
}

/**
 * ai
 * 
 * Adjusts the velocity / state of the NPC
 * to follow the player
 * 
 */
void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
        // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    // if player 'in range' stop and fire
    if(distance < maxRange)
    {
        if (NPCcooldownTimer > NPC_COOLDOWN_MAX)
        {
            player->takeHealth();
            game->printHealth();
            NPCcooldownTimer = 0;
        }
    }
    else
    {
        // else - head for player

        // Could do with an assign in vector
        velocity->setX(vectorToPlayer.getX());
        velocity->setY(vectorToPlayer.getY());   
        
        // will work but 'wobbles'
        //velocity->scale(speed);   

        // Arrive pattern 
        velocity->scale(timeToTarget);

        if(velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);            
        }

        state = IDLE;

        if(velocity->getY() > 0.1f)
            state = DOWN;
        else
            if(velocity->getY() < -0.1f)
                state = UP;
            // empty else is not an error!
        
        if(velocity->getX() > 0.1f)
            state = RIGHT;
        else
            if(velocity->getX() < -0.1f)
                state = LEFT;
            // empty else is not an error. 
    }
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    return state;
}

void NPC::takeDamage(int damage)
{
    health -= damage;

    if(health <= 0)
        state = DEAD;
}

bool NPC::isDead()
{
    if (health > 0)
        return false;
    else 
        return true;

}

void NPC::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
    health = 100;
    state = IDLE;

    Vector2f randomPostion;

    int doubleWidth = MAX_WIDTH * 1.2f;
    int doubleHeight = MAX_HEIGHT *1.2f;

    // get a random number between 0 and 
    // 2 x screen size. 
    int xCoord = rand()%doubleWidth;
    int yCoord = rand()%doubleHeight;

    // if its on screen move it off. 
    if(xCoord < MAX_WIDTH)
        xCoord *= -1;

    if(yCoord < MAX_HEIGHT)
        yCoord *= -1; 

    randomPostion.setX(xCoord);
    randomPostion.setY(yCoord);

    this->setPosition(&randomPostion);    
}
    
int NPC::getPoints()
{
    return points;
}

void NPC::setPoints(int pointValue)
{
    points = pointValue;
}
    