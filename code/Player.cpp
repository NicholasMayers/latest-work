#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file
const float Player::COOLDOWN_MAX = 0.2f;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
    state = IDLE_DOWN;
    speed = 100.0f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Initialise weapon cooldown. 
    cooldownTimer = 0;
    BulletDirY = B_DOWN;
    BulletDirX = 0;

    // Init points
    points = 0;

    health = 100;
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/mainChar5.png");

    //postion
    Vector2f position(200.0f,200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 8, &position);

    // Setup the animation structure
    animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE_DOWN]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE_UP]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[IDLE_LEFT]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[IDLE_RIGHT]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{
}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;
    // If no keys are down player should not animate!
    //bullets should fire where player is looking
    switch (state)
    {
        case UP:
            state = IDLE_UP;
            BulletDirY = B_UP;
            BulletDirX = 0;
            break;
        case DOWN:
            state = IDLE_DOWN;
            BulletDirX = 0;
            BulletDirY = B_DOWN;
            break;
        case LEFT:
            state = IDLE_LEFT;
            BulletDirX = B_LEFT;
            BulletDirY = 0;
            break;
        case RIGHT:
            state = IDLE_RIGHT;
            BulletDirX = B_RIGHT;
            BulletDirY = 0;
            break;

        default:
        break;
    }

    // This could be more complex, e.g. increasing the vertical
    // input while the key is held down.
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
        BulletDirY = B_UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
        BulletDirY = B_DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
        BulletDirX = B_RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
        BulletDirX = B_LEFT;
    }

    // Calculate player velocity.
    velocity->setY(verticalInput);
    velocity->setX(horizontalInput);
    velocity->normalise();
    velocity->scale(speed);

    if (keyStates[SDL_SCANCODE_SPACE])
    {
        fire();
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;

}

void Player::fire()
{   
    // Need a cooldown timer, otherwise we shoot 
    // a million bullets a second and our npc
    // dies instantly

    if(cooldownTimer > COOLDOWN_MAX)
    {
        cooldownTimer = 0;
        game->createBullet(position, BulletDirX, BulletDirY);
    }  
}

void Player::addScore(int points)
{
    this->points += points;
}

int Player::getScore()
{
    return points;
}

void Player::takeHealth()
{
    this->health -= 20;
}

int Player::getHealth()
{
    return health;
}
