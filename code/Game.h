#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"

#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class Vector2f;

class Game 
{
private:
    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;

    // Background texture
    SDL_Texture*    backgroundTexture;

    //Declare Player
    Player*         player;

    //Declare NPC
    NPC* npcs [100];

    // Bullets
    vector<Bullet*> bullets;
    
    // Keyboard
    const Uint8     *keyStates;

    // Window control
    bool quit;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

    int Waves;

    int ActiveNPCs;

    int NPCmax;

    int NPCdead;

public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    void createBullet(Vector2f *position, float Xdirection, float Ydirection);
    void collisionDetection();

    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 800;

    void nextWave();

    void printHealth();

    void highScore();
};

#endif