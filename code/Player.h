#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 39;
    static const int SPRITE_WIDTH = 39;

    // Bullet spawn
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    Game* game;

    // Score
    int points;

    int health;

    int BulletDirX;
    int BulletDirY;

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT, RIGHT, UP, DOWN, IDLE_DOWN, IDLE_UP, IDLE_LEFT, IDLE_RIGHT};

    enum BulletDir
    {
        B_LEFT = -1,
        B_RIGHT = 1,
        B_UP = -1,
        B_DOWN = 1
    };

    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);

    void setGame(Game* game);
    void fire();

    int getCurrentAnimationState();

    //Overloads
    void update(float timeDeltaInSeconds);

    void addScore(int points);
    int getScore();

    void takeHealth();
    int getHealth();
};

#endif