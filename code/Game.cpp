#include "Game.h"
#include "TextureUtils.h"

#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "Vector2f.h"
#include "AABB.h"

//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>

#include <iostream>
#include <fstream>
using namespace std;

Game::Game() 
{
    gameWindow = nullptr;
    gameRenderer = nullptr;

    backgroundTexture = nullptr;
    player = nullptr;

    for (int i = 0; i < 100; i++)
        npcs[i] = nullptr;

    keyStates = nullptr;
    quit = false;

    //current enemy waves
    Waves = 1;

    ActiveNPCs = 0;

    NPCmax = 1;
}

void Game::init()
{
     gameWindow = SDL_CreateWindow("Bullet Flood",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }
    
    // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);

    if(backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);

    for (int i = 0; i < NPCmax; i++)
    {
        npcs[i] = new NPC();
        npcs[i]->init(gameRenderer);
        npcs[i]->setGame(this);
        ActiveNPCs++;
    }

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;

    for (int i = 0; i < ActiveNPCs; i++)
    {
        delete npcs[i];
        npcs[i] = nullptr;
        free(npcs[i]);
    }

    for (vector<Bullet*>::iterator bullet = bullets.begin() ; bullet != bullets.end();)
    {   
        delete *bullet;
        *bullet = nullptr;
        bullet = bullets.erase(bullet);
    } 
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Game::draw()
{
     // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    //new SDL_Rect{0, 0, 160, 160}
    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, 
            NULL, 
            NULL);

    player->draw(gameRenderer);

    for (int i = 0; i < ActiveNPCs; i++)
    {
        npcs[i]->draw(gameRenderer);
    }

    for (long unsigned int i = 0; i < bullets.size(); i++)
        bullets[i]->draw(gameRenderer);

    // 2.5 Draw HUD
    //
    //printf("Player Score: %d\n", player->getHealth());

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);
    
}
void Game::update(float timeDelta)
{
    player->update(timeDelta);

    int deadNPCs = 0;

    for (int i = 0; i < ActiveNPCs; i++)
    {
        (npcs[i])->update(timeDelta);

        if ((npcs[i])->isDead())
        {
            deadNPCs++;

            if (deadNPCs == ActiveNPCs)
                nextWave();
        }
    }

    
    for (vector<Bullet*>::iterator bullet = bullets.begin(); bullet != bullets.end();)
    {
        if((*bullet)->hasExpired())
        {
            delete *bullet;
            *bullet = nullptr;
            bullet = bullets.erase(bullet);
        }
        else
        {
            (*bullet)->update(timeDelta);
            ++bullet;
        }
    }

    collisionDetection();

    if (player->getHealth() <= 0)
    {
        printf("Game Over!\n");
        printf("Final Score: %d\n", player->getScore());

        highScore();
    }
}

void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;    
        }
    }

    // Process Inputs - for player 
    player->processInput(keyStates);
}

Player* Game::getPlayer() 
{
    return player;
}

void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}

void Game::createBullet(Vector2f *position, float Xdirection, float Ydirection)
{
    Bullet* bullet = new Bullet();
    
    Vector2f* Bvol = new Vector2f();
    Bvol->setY(Ydirection);
    Bvol->setX(Xdirection);

    bullet->init(gameRenderer, position, Bvol);    

    delete Bvol;
    Bvol = nullptr;

    bullets.push_back(bullet);
}

void Game::collisionDetection()
{
    for (int i = 0; i < ActiveNPCs; i++)
    {
        if (!(npcs[i]->isDead())) //shouldn't interact if the npc is dead!
        {
            for (vector<Bullet *>::iterator bullet = bullets.begin(); bullet != bullets.end();)
            {
                if ((*bullet)->getAABB()->intersects((npcs[i])->getAABB()))
                {
                    // increase player score
                    player->addScore((npcs[i])->getPoints());
                    printf("Player Score: %d\n", player->getScore());

                    // damage npc
                    (npcs[i])->takeDamage((*bullet)->getDamage());

                    // destroy arrow (or it'll keep hitting!)
                    delete *bullet;
                    *bullet = nullptr;
                    bullet = bullets.erase(bullet);
                    
                }
                else
                {
                    ++bullet;
                }
            }
        }
    }
}

void Game::nextWave()
{
    Waves++;
    printf("Wave: %d \n", Waves);

    for (int i = 0; i < ActiveNPCs; i++)
        npcs[i]->respawn(WINDOW_HEIGHT,WINDOW_WIDTH);

    if (NPCmax < 100)
        NPCmax += 3;

    for (int i = ActiveNPCs; i < NPCmax; i++)
    {
        npcs[i] = new NPC();
        npcs[i]->init(gameRenderer);
        npcs[i]->setGame(this);
        ActiveNPCs++;
    }
}

void Game::printHealth()
{
    printf("Player Health: %d\n", player->getHealth());
}

void Game::highScore()
{
    fstream score_file;

    score_file.open("assets/score_file.txt", ios::in);

    if (!score_file)
    {
        cout << "No such file";
    }
    else
    {
        int highscore;
        int score = player->getScore();
        
        score_file >> highscore;

        if (highscore < score)
        {
            std::cout << R"( 
            __    __ ________ __       __      __    __ ______  ______  __    __  ______   ______   ______  _______  ________ __ __ __ 
            |  \  |  \        \  \  _  |  \    |  \  |  \      \/      \|  \  |  \/      \ /      \ /      \|       \|        \  \  \  \
            | ▓▓\ | ▓▓ ▓▓▓▓▓▓▓▓ ▓▓ / \ | ▓▓    | ▓▓  | ▓▓\▓▓▓▓▓▓  ▓▓▓▓▓▓\ ▓▓  | ▓▓  ▓▓▓▓▓▓\  ▓▓▓▓▓▓\  ▓▓▓▓▓▓\ ▓▓▓▓▓▓▓\ ▓▓▓▓▓▓▓▓ ▓▓ ▓▓ ▓▓
            | ▓▓▓\| ▓▓ ▓▓__   | ▓▓/  ▓\| ▓▓    | ▓▓__| ▓▓ | ▓▓ | ▓▓ __\▓▓ ▓▓__| ▓▓ ▓▓___\▓▓ ▓▓   \▓▓ ▓▓  | ▓▓ ▓▓__| ▓▓ ▓▓__   | ▓▓ ▓▓ ▓▓
            | ▓▓▓▓\ ▓▓ ▓▓  \  | ▓▓  ▓▓▓\ ▓▓    | ▓▓    ▓▓ | ▓▓ | ▓▓|    \ ▓▓    ▓▓\▓▓    \| ▓▓     | ▓▓  | ▓▓ ▓▓    ▓▓ ▓▓  \  | ▓▓ ▓▓ ▓▓
            | ▓▓\▓▓ ▓▓ ▓▓▓▓▓  | ▓▓ ▓▓\▓▓\▓▓    | ▓▓▓▓▓▓▓▓ | ▓▓ | ▓▓ \▓▓▓▓ ▓▓▓▓▓▓▓▓_\▓▓▓▓▓▓\ ▓▓   __| ▓▓  | ▓▓ ▓▓▓▓▓▓▓\ ▓▓▓▓▓   \▓▓\▓▓\▓▓
            | ▓▓ \▓▓▓▓ ▓▓_____| ▓▓▓▓  \▓▓▓▓    | ▓▓  | ▓▓_| ▓▓_| ▓▓__| ▓▓ ▓▓  | ▓▓  \__| ▓▓ ▓▓__/  \ ▓▓__/ ▓▓ ▓▓  | ▓▓ ▓▓_____ __ __ __ 
            | ▓▓  \▓▓▓ ▓▓     \ ▓▓▓    \▓▓▓    | ▓▓  | ▓▓   ▓▓ \\▓▓    ▓▓ ▓▓  | ▓▓\▓▓    ▓▓\▓▓    ▓▓\▓▓    ▓▓ ▓▓  | ▓▓ ▓▓     \  \  \  \
             \▓▓   \▓▓\▓▓▓▓▓▓▓▓\▓▓      \▓▓     \▓▓   \▓▓\▓▓▓▓▓▓ \▓▓▓▓▓▓ \▓▓   \▓▓ \▓▓▓▓▓▓  \▓▓▓▓▓▓  \▓▓▓▓▓▓ \▓▓   \▓▓\▓▓▓▓▓▓▓▓\▓▓\▓▓\▓▓                                                                               
            )";

            score_file << score;
        }
    }
    score_file.close();
    quit = true;
}